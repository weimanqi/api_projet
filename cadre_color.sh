iLeft=3
iTop=2
cBorder=$cGreen
cScore=$cFuchsia
cScoreValue=$cCyan
((iTrayLeft = iLeft + 2))
((iTrayTop = iTop + 1))
((iTrayWidth = 10))
((iTrayHeight = 15))
function InitDraw()
{
        clear
        #RandomBox        #随机产生方块，这时右边预显示窗口中有方快了
        #RandomBox        #再随机产生方块，右边预显示窗口中的方块被更新，原先的方块将开始下落
        local i t1 t2 t3

        #显示边框
        #不是一行行孤立的！！！是一个整体 上下文有联系的！！！最后用/033[0m做总结
        echo -ne "\033[1m" #亮度？
        echo -ne "\033[3${cBorder}m\033[4${cBorder}m" #边框用绿色的：背景和字体都设成绿色的 那么后面所有的都是这个颜色了 直到 \033[0m

        ((t2 = iLeft + 1))#左边
        ((t3 = iLeft + iTrayWidth * 2 + 3))#右边的边框 不用细究
        for ((i = 0; i < iTrayHeight; i++))
        do
                ((t1 = i + iTop + 2))#高度需要变
                echo -ne "\033[${t1};${t2}H||"#\33[y;xH设置光标位置再写一个||
                echo -ne "\033[${t1};${t3}H||"
        done

        ((t2 = iTop + iTrayHeight + 2))
        for ((i = 0; i < iTrayWidth + 2; i++))
        do
                ((t1 = i * 2 + iLeft + 1))
                echo -ne "\033[${iTrayTop};${t1}H=="
                echo -ne "\033[${t2};${t1}H=="
        done
        echo -ne "\033[0m"


        #显示"Score"和"Level"字样
        echo -ne "\033[1m"
        ((t1 = iLeft + iTrayWidth * 2 + 7))
        ((t2 = iTop + 10))
        echo -ne "\033[3${cScore}m\033[${t2};${t1}HScore"
        ((t2 = iTop + 11))
        echo -ne "\033[3${cScoreValue}m\033[${t2};${t1}H${iScore}"
        echo -ne "\033[0m"
}
