#!/bin/bash


pos_h=(0) # position_horizontal 
pos_v=(0) # position_vertical : apres creer la balle, on change pas
taille=(0) # tallie de la balle
nb_b=0 # nb_balle

# les variables besoins
score=0

function rand()
{
    min=$1
    max=$(($2-$min+1))
    num=$(($RANDOM+1000000000)) 
    echo $(($num%$max+$min))
}

function creer_ball()
{
(($nb_b++))
pos_h[$nb_b]=0
pos_v[$nb_b]=0
rnd=$(rand 1 3)
taille[$nb_b]=$rnd
}

function tombe()
{
((pos_h[$1]++))

}

#function collision(){}

#function creer_un_plus_grand_balle(){}

function supprimer_balle()
{
if [ taille[$1] = taille[$2] ]
then
	for i in {$1..$nb_b}
	do
		pos_h[$i]=$(pos_h[$(i+1)])
		pos_v[$i]=$(pos_v[$(i+1)])
		taille[$i]=$(taille[$(i+1)])
		(($nb_b--))
	done
	for i in {$2..$nb_b}
        do 
                pos_h[$i]=$(pos_h[$(i+1)])
                pos_v[$i]=$(pos_v[$(i+1)])
                taille[$i]=$(taille[$(i+1)])
                (($nb_b--))
        done
fi

}

#function collision_balle(){}

#function score(){}

#function echec(){}

#function reussir(){}
